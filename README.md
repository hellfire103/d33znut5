# D33ZNUT5

## Introduction
D33ZNUT5 is a soundboard app featuring comedic and meme sounds from 2022.

## Screenshot
![Screenshot1](https://i.ibb.co/T0t7cVh/Screenshot-2022-12-01-20-37-04.png)

## Installation
Currently, the only way to install D33ZNUT5 is by downloading the APK and installing it manually. The APK can be found in the files above, as GitLab doesn't allow binaries in the Releases tab, unlike GitHub, Gitea, Codeberg, SourceHut, SourceForge... Ya know, EVERY OTHER GIT HOSTING PLATFORM!?

## Usage
Once the app is open, simply tap the images to play the sounds. Tap the switch to loop the sound, and hit "Emergency Stop" if anything goes wrong.

## Contributing
I'm not looking for any other developers to work on this right now, but feel free to open an issue if you find a bug or want to request a feature.

## Authors and acknowledgment
### Developer
- Edan Osborne

### Ideas
- Rhiannon Ward
- Felix Hayler-Hughes
- Harry Batey
- Lucas Grant
- Lewis Davison
- Elliot Jenkins
- Tom Hindle

### All media included (such as images and sounds) is the work of their respective authors and is used under fair use.

## License
This app is licensed under the GNU General Public License v3.0
